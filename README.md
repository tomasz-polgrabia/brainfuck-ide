# Purpose

It was a test project for java studies (to complete the course). Except of putting it under the gradle control (it
wasn't), whole code is original from those times. I will try to translate original comments, naming convention to
non-polish.

# Technology

Ancient swing gui with some custom helpers from other authors

- https://github.com/sporst/splib
- https://github.com/Konloch/bytecode-viewer
  HexEditor (com.jhe.hexed) and other (tv.ports.*) are not mine, just reused.
